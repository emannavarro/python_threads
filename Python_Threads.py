import logging
import threading
import time


def thread_one_function():
  for number in range(0, 11):
    print("thread one",number)
    time.sleep(1)


def thread_two_function():
  for number in range(10, -1, -1):
    print("thread two",number)
    time.sleep(1)




if __name__ == "__main__":
  x = threading.Thread(target=thread_one_function)
  x.start()
  y = threading.Thread(target=thread_two_function)
  y.start()